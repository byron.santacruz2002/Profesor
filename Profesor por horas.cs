﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByronSantacruz3BProfesor
{
    public class Profesor_por_horas : Profesor
    {
        private int Precio_H;
        private int H_Trabajadas;
        public int getPrecio_H()
        {
            return Precio_H;
        }
        public void setPrecio_H(int Precio_H)
        {
            this.Precio_H = Precio_H;
        }
        public int getH_Trabajadas()
        {
            return H_Trabajadas;
        }
        public void setH_Trabajadas(int H_Trabajadas)
        {
            this.H_Trabajadas = H_Trabajadas;
        }
        public override void Sueldo()
        {
            int Sueldo;
            Sueldo = Precio_H * H_Trabajadas;
            Console.WriteLine("El sueldo que recibe es de: $"+Sueldo);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByronSantacruz3BProfesor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Profesor por horas
            Profesor_por_horas profesor_Hora = new Profesor_por_horas();
            profesor_Hora.setNombre("Byron");
            profesor_Hora.setApellidos("Santacruz");
            profesor_Hora.setDirección("Cañita");
            profesor_Hora.setCédula(1315586477);
            profesor_Hora.setH_Trabajadas(15);
            profesor_Hora.setPrecio_H(12);

            //Profesor por contrato
            Profesor_por_contrato profesor_Contrato = new Profesor_por_contrato();
            profesor_Contrato.setNombre("Flerida");
            profesor_Contrato.setApellidos("Saltos");
            profesor_Contrato.setDirección("Charapoto");
            profesor_Contrato.setCédula(1310525686);
            profesor_Contrato.setH_Extras(45);
            profesor_Contrato.setSueldoBásico(155);

            //Profesor por nombramiento
            Profesor_con_nombramiento profesor_Nombramiento = new Profesor_con_nombramiento();
            profesor_Nombramiento.setNombre("Nohelia");
            profesor_Nombramiento.setApellidos("Santacruz");
            profesor_Nombramiento.setDirección("Bahia");
            profesor_Nombramiento.setCédula(1315586964);
            profesor_Nombramiento.setsueldofijo(1500);

            List<Profesor> ListaProfesor = new List<Profesor>();
            ListaProfesor.Add(profesor_Hora);
            ListaProfesor.Add(profesor_Contrato);
            ListaProfesor.Add(profesor_Nombramiento);
            foreach (Profesor profesor in ListaProfesor)
            {
                profesor.Mostrar();
                profesor.Sueldo();
                Console.ReadLine();
            }
        }
    }
}

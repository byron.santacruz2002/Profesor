﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByronSantacruz3BProfesor
{
    public class Profesor_con_nombramiento : Profesor
    {
        private int SueldoFijo;
        public int getsueldofijo()
        {
            return SueldoFijo;
        }
        public void setsueldofijo(int SueldoFijo)
        {
            this.SueldoFijo = SueldoFijo;
        }

        public override void Sueldo()
        {
            if(SueldoFijo > 1000)
            {
                Console.WriteLine("Su sueldo a recibir es de: $" + SueldoFijo);
            }
            else if (SueldoFijo <= 1000)
            {
                Console.WriteLine("No es valido esta operacion");
            }

        }
    }
}

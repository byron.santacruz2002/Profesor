﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByronSantacruz3BProfesor
{
    public abstract class Profesor
    {
        private string Nombre;
        private string Apellidos;
        private string Dirección;
        private int Cédula;
        public string getNombre()
        {
            return Nombre;
        }
        public void setNombre(string Nombre)
        {
            this.Nombre = Nombre;
        }
        public string getApellidos()
        {
            return Apellidos;
        }
        public void setApellidos(string Apellidos)
        {
            this.Apellidos = Apellidos;
        }
        public string getDirección()
        {
            return Dirección;
        }
        public void setDirección(string Dirección)
        {
            this.Dirección = Dirección;
        }
        public int getCédula()
        {
            return Cédula;
        }
        public void setCédula(int Cédula)
        {
            this.Cédula = Cédula;
        }
        public  void Mostrar()
        {
            Console.WriteLine("Nombre: {0}\nApellido: {1}\nDireccion:{2}\nCedula:{3}",Nombre,Apellidos,Dirección,Cédula);  
        }
        public abstract void Sueldo();

    }
}

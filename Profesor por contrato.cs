﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByronSantacruz3BProfesor
{
    public class Profesor_por_contrato : Profesor
    {
        private int SueldoBásico;
        private int H_Extras;
        public int getSueldoBásico()
        {
            return SueldoBásico;
        }
        public void setSueldoBásico(int SueldoBásico)
        {
            this.SueldoBásico = SueldoBásico;
        }
        public int getH_Extras()
        {
            return SueldoBásico;
        }
        public void setH_Extras(int H_Extras)
        {
            this.H_Extras = H_Extras;
        }
        public override void Sueldo()
        {
            int Sueldo;
            Sueldo = SueldoBásico - H_Extras;
            Console.WriteLine("El sueldo que recibe es de: $" + Sueldo);
        }
    }
}
